# Known API
https://www.masterani.me/api/anime/55-no-game-no-life/
https://www.masterani.me/api/anime/55
https://www.masterani.me/api/anime/55/detailed

## Can go to anime page like
https://www.masterani.me/anime/watch/55-no-game-no-life/4

## Get embeded with 
Curl the page and get the video-mirror tag 
`
<video-mirrors :mirrors='[{"id":265295,"host_id":1,"embed_id":"ud86a6lujppn","quality":1080,"type":1,"host":{"id":1,"name":"MP4Upload","embed_prefix":"https:\/\/mp4upload.com\/embed-","embed_suffix":".html"}},{"id":265319,"host_id":1,"embed_id":"ljr1pcviarcn","quality":720,"type":1,"host":{"id":1,"name":"MP4Upload","embed_prefix":"https:\/\/mp4upload.com\/embed-","embed_suffix":".html"}},{"id":265251,"host_id":18,"embed_id":"obbreeclolsdsemt","quality":720,"type":1,"host":{"id":18,"name":"Streamango","embed_prefix":"https:\/\/streamango.com\/embed\/","embed_suffix":null}},{"id":236564,"host_id":14,"embed_id":"NDMyODI","quality":720,"type":1,"host":{"id":14,"name":"Vidstreaming","embed_prefix":"https:\/\/vidstreaming.io\/streaming.php?id=","embed_suffix":null}},{"id":265307,"host_id":1,"embed_id":"176zqav4dsda","quality":480,"type":1,"host":{"id":1,"name":"MP4Upload","embed_prefix":"https:\/\/mp4upload.com\/embed-","embed_suffix":".html"}},{"id":265271,"host_id":18,"embed_id":"aroptfmftrppbrmk","quality":480,"type":1,"host":{"id":18,"name":"Streamango","embed_prefix":"https:\/\/streamango.com\/embed\/","embed_suffix":null}}]'></video-mirrors>
`

## Take information
custom id with a json list , you just need too format the url like `https://mp4upload.com/embed-ud86a6lujppn.html` , that is, embed_prefix + embed_id + embed_suffix 

## Within embeded
`
image|mp4|video|q2x3bxdgz3b4quuoyouq6nqyc5zw7evigayxqt5bjba62b2qhzqk7e4z|
`
then use the code in :
`
https://www13.mp4upload.com:282/d/{{HERE}}/video.mp4
`

## Play video
Download with link or stream it through a player like mpv
