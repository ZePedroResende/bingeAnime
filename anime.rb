require 'json'
require 'net/http'
require 'nokogiri'

class Anime
  def self.get_anime_info(id)
    response = Net::HTTP.get(URI("https://www.masterani.me/api/anime/#{id}"))
    JSON.parse(response)
  end

  def self.get_video_url(slug,epi)
    url = "https://www.masterani.me/anime/watch/#{slug}/#{epi}"
    response = Net::HTTP.get(URI(url))
    document = Nokogiri::HTML.parse(response)
    mirrors_list = JSON.parse(document.css('video-mirrors')[0][":mirrors"])
    mirror = mirrors_list[0] 
    url_emb = "#{mirror["host"]["embed_prefix"]}#{mirror["embed_id"]}#{mirror["host"]["embed_suffix"]}"
    response_emb = Net::HTTP.get(URI(url_emb))
    url = /video\|(.+)\|282/.match(response_emb)
    www_number = /\|quot\|.+\|(ww.+)\|complete\|/.match(response_emb)
    "https://#{www_number[1]}.mp4upload.com:282/d/#{url[1]}/video.mp4"
  end

  def self.list_episodes(id)
    info = get_anime_info(id)
    slug = info["slug"]
    episode_count = info["episode_count"]
    (5..episode_count).each do |epi|
      puts "#{epi} #{get_video_url(slug,epi)}"
      #puts selenium_video_url(slug,epi)
    end
  end
end

Anime.list_episodes(ARGV[0])
